import React, { Component } from "react";
import { Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

class ProductCard extends Component {
  render() {
    return (
      <div className="mt-3 col-xl-3 col-lg-6 col-md-6 col-sm-13 col-12">
        <Card style={{ width: "auto", border: "0" }}>
          <Card.Img
            style={{ height: "250px" }}
            variant="top"
            src={this.props.product.src}
          />
          <Card.Body>
            <Card.Title>{this.props.product.name}</Card.Title>
            <Card.Text>{this.props.product.des}</Card.Text>
            <Button
              variant=""
              style={{ text: "black", border: "0.5px solid black" }}
            >
              <Link
                to={`/posts/` + this.props.product.id}
                style={{ textDecoration: "none" }}
              >
                See More
              </Link>
            </Button>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default ProductCard;
