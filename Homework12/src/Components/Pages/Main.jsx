import React from "react";
import { Carousel } from "react-bootstrap";
export default function Main() {
  return (
    <div>
      <Carousel>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://ncxhonda.com.kh/wp-content/uploads/2020/05/UrBaN-1920-x-900.png"
            alt="Third slide"
          />
          <Carousel.Caption></Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://ncxhonda.com.kh/wp-content/uploads/2020/05/Prestige-1920-x-900.png"
            alt="Third slide"
          />

          <Carousel.Caption></Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://ncxhonda.com.kh/wp-content/uploads/2020/05/Club12-1920-x-900.png"
            alt="First slide"
          />
          <Carousel.Caption></Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </div>
  );
}
