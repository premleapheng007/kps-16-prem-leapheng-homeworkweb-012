import { Container, Form, Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import auth from "./Route/auth";
import React from "react";
export default function Auth(props) {
  let histroy = useHistory();
  let username = "";
  let pass = "";
  function getUsername(event) {
    username = event.target.value;
  }
  function getPass(event) {
    pass = event.target.value;
  }
  function myRedirect() {
    if (username !== "" && pass !== "") {
      auth.login(() => {
        histroy.push("/welcome");
      });
    }
  }
  return (
    <Container>
      <div className="form mt-3">
        <Form.Group>
          <Form.Control
            type="text"
            placeholder="Username"
            onChange={(e) => getUsername(e)}
          />
        </Form.Group>
        <Form.Group>
          <Form.Control
            type="Password"
            placeholder="Password"
            onChange={(e) => getPass(e)}
          />
        </Form.Group>
        <div style={{ textAlign: "center" }}>
          <Button
            variant=""
            style={{ border: "1px solid black" }}
            onClick={() => myRedirect()}
          >
            Submit
          </Button>
        </div>
      </div>
    </Container>
  );
}
