import React from "react";
import {
  Link,
  BrowserRouter,
  Switch,
  Route,
  useRouteMatch,
} from "react-router-dom";
import { Container } from "react-bootstrap";
import Topic from "./Topic";
export default function MovieCategory() {
  let { path, url } = useRouteMatch();
  return (
    <Container>
      <div>
        <BrowserRouter>
          <h4>Movie Category</h4>
          <ul>
            <li>
              <Link to={`${url}/adventure`}>Adventure</Link>
            </li>
            <li>
              <Link to={`${url}/comedy`}>Comedy</Link>
            </li>
            <li>
              <Link to={`${url}/crime`}>Crime</Link>
            </li>
            <li>
              <Link to={`${url}/documentary`}>Documentary</Link>
            </li>
          </ul>
          <Switch>
            <Route exact path={path}>
              <h3>Please Select A topic</h3>
            </Route>
            <Route path={`${path}/:topicName`}>
              <Topic></Topic>
            </Route>
          </Switch>
        </BrowserRouter>
      </div>
    </Container>
  );
}
