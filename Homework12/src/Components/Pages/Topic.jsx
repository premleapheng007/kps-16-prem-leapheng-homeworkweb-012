import React from "react";
import { useParams } from "react-router-dom";
export default function Topic() {
  let { topicName } = useParams();
  return (
    <div>
      <h3>{topicName}</h3>
    </div>
  );
}
